<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function index()
    {
        return view('client.pages.index');
    }
    public function docs()
    {
        return view('client.pages.docs');
    }
    public function changelog()
    {
        return view('client.pages.changelog');
    }
}
