<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>Document</title>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1>user</h1>
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">user name</th>
                            <th scope="col">email</th>
                            <th scope="col">action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($user as $item)
                        <tr>
                            <td>{{$item -> username}}</td>
                            <td>{{$item -> email}}</td>
                            <form action="{{route('admin.user.destroy',['id'=>$item->id])}}" method="post">
                                <td><input class="btn btn-default" type="submit" value="Delete" /></td>
                                <td><input type="hidden" name="_method" value="delete" /></td>
                                {!! csrf_field() !!}
                            </form>
                            <td><button><a href="{{route('admin.user.edit',['id'=>$item->id])}}">edit</a></button></td>
                            </tr>
                        @endforeach
                        <td><button><a href='{{route('admin.user.create')}}'>thêm</a></button></td>
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</body>
</html>