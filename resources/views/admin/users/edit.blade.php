
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <form action="{{ route('admin.user.update', ['id' => $user->id]) }}" method="post">
                    {!! csrf_field() !!}
                    {{ method_field('PUT') }}
                    <label for="">username</label>
                    <input type="text" class="form-control"  placeholder="{{ $user->username }}" name="username" >
                    <label for="">email</label>
                    <input type="text" class="form-control"  placeholder="{{ $user->email }}" name="email" >
                    <label for="">password</label>
                    <input type="password" class="form-control" placeholder="{{ $user->passworld }}" name="passworld" >
                    <button class="btn btn-success w-100 mt-2">GO</button>
                </form>
            </div>
        </div>
    </div>
    

</body>
</html>