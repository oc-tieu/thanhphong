
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <form action="{{ route('admin.category.update', ['id' => $category->id]) }}" method="post">
                    {!! csrf_field() !!}
                    {{ method_field('PUT') }}
                    <label for="">name</label>
                    <input type="text" class="form-control"  placeholder="{{ $category->name }}" name="name" >
                    <button class="btn btn-success w-100 mt-2">GO</button>
                </form>
            </div>
        </div>
    </div>
    

</body>
</html>