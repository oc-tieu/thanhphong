<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>Document</title>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1>user</h1>
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">name</th>
                            <th scope="col">price</th>
                            <th scope="col">intro</th>
                            <th scope="col">detail</th>
                            <th scope="col">img</th>
                            <th scope="col">action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($product as $item)
                        <tr>
                            <td>{{$item->name}}</td>
                            <td>{{$item->price}}</td>
                            <td>{{$item->intro}}</td>
                            <td>{{$item->detail}}</td>
                            <td>@foreach($item->imgs as $imgs) <img width="150px" height="150px" src="{{asset($imgs->url)    }}" alt="" srcset=""> @endforeach</td>
                            <form action="{{route('admin.product.destroy',['id'=>$item->id])}}" method="post">
                                <td><input class="btn btn-default" type="submit" value="Delete" /></td>
                                <td><input type="hidden" name="_method" value="delete" /></td>
                                {!! csrf_field() !!}
                            </form>
                            <td><button><a href="{{route('admin.product.edit',['id'=>$item->id])}}">edit</a></button></td>
                            </tr>
                        @endforeach
                        <td><button><a href='{{route('admin.product.create')}}'>thêm</a></button></td>
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</body>
</html>