
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <form action="{{route('admin.product.store')}}" method="post" enctype="multipart/form-data" >
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <label for="">name</label>
                    <input type="text" class="form-control" name="name" >
                    <label for="">price</label>
                    <input type="text" class="form-control" name="price" >
                    <label for="">intro</label>
                    <input type="text" class="form-control" name="intro" >
                    <label for="">detail</label>
                    <input type="text" class="form-control" name="detail" >
                    <br>
                    <select class="mdb-select md-form w-100" name="category_id">
                        <option value="" disabled selected>Danh Mục</option>
                        @foreach ($category as $item)
                            <option value="{{$item->id}}" name="category_id">{{$item->name}}</option>
                            
                        @endforeach
                    </select>
                    <br>
                    <div class="form-group">
                        <label for="exampleFormControlFile1">Example file input</label>
                        <input type="file" class="form-control-file" id="exampleFormControlFile1" name="url" >
                    </div>
                    <button class="btn btn-success w-100 mt-2">GO</button>
                </form>
            </div>
        </div>
    </div>
    

</body>
</html>