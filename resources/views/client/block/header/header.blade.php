<header class="uk-background-secondary">
    <div data-uk-sticky="sel-target: .uk-navbar-container; cls-active: uk-navbar-sticky" class="uk-sticky uk-sticky-fixed" style="position: fixed; top: 0px; width: 1904px;">
        <nav class="uk-navbar-container">
            <div class="uk-container">
                <div data-uk-navbar>
                    <div class="uk-navbar-left">
                        <a class="uk-navbar-item uk-logo uk-visible@m" href="{{route('index')}}"><img src="{{asset('client/posts/logo.svg')}}" alt="Docs"></a>
                        <a class="uk-navbar-toggle uk-hidden@m" href="#offcanvas-docs" data-uk-toggle><span data-uk-navbar-toggle-icon></span> <span class="uk-margin-small-left">Navigate</span></a>
                    </div>
                    <div class="uk-navbar-center uk-hidden@m">
                    <a class="uk-navbar-item uk-logo" href="{{route('index')}}"><img src="{{asset('client/posts/logo.svg')}}" alt="Docs"></a>
                    </div>
                    <div class="uk-navbar-right">
                        <ul class="uk-navbar-nav uk-visible@m">
                            <li class="uk-active"><a href="index.html" >Help</a></li>
                            <li><a href="news/index.html" >News</a></li>
                        <li><a href="{{route('changelog')}}" >Changelog</a></li>
                            <li><a href="contact/index.html" >Contact</a></li>
                        </ul>
                        <div>
                            <a class="uk-navbar-toggle" uk-search-icon href="#"></a>
                            <div class="uk-drop uk-background-default uk-border-rounded" uk-drop="mode: click; pos: left-center; offset: 0">
                                <form class="uk-search uk-search-navbar uk-width-1-1" onsubmit="return false;">
                                    <input id="navbar-search" class="uk-search-input" type="search" placeholder="Search..." autofocus>
                                </form>
                                <ul id="navbar-search-results" class="uk-position-absolute uk-width-1-1 uk-list"></ul>
                            </div>
                        </div>
                        <script>
                        SimpleJekyllSearch({
                            searchInput: document.getElementById('navbar-search'),
                            resultsContainer: document.getElementById('navbar-search-results'),
                            noResultsText: '<li>No results found</li>',
                            searchResultTemplate: '<li><a href="{url}">{title}</a></li>',
                            json: '/search.json'
                        });
                        </script>
                        <a class="uk-navbar-toggle uk-hidden@m" href="#offcanvas" data-uk-toggle><span data-uk-navbar-toggle-icon></span> <span class="uk-margin-small-left">Menu</span></a>
                    </div>
                </div>
            </div>
        </nav>
    </div>
</header>