<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Begin Jekyll SEO tag v2.5.0 -->
    <title>Docs | Documentation Jekyll theme.</title>
    <meta name="generator" content="Jekyll v3.7.4" />
    <meta property="og:title" content="Docs" />
    <meta name="author" content="John Smith" />
    <meta property="og:locale" content="en_US" />
    <meta name="description" content="Documentation Jekyll theme." />
    <meta property="og:description" content="Documentation Jekyll theme." />
    <link rel="canonical" href="index.html" />
    <meta property="og:url" content="index.html" />
    <meta property="og:site_name" content="Docs" />
    <script type="application/ld+json">
    {"@type":"WebSite","headline":"Docs","url":"https://docs.jekyll.plus/","name":"Docs","author":{"@type":"Person","name":"John Smith"},"description":"Documentation Jekyll theme.","@context":"http://schema.org"}</script>
    <!-- End Jekyll SEO tag -->
    
    <meta property="og:image" content="https://docs.jekyll.plus/assets/posts/logo.svg"/>
    <link rel="stylesheet" href="{{asset('client/css/main.css')}}">
    <link rel="shortcut icon" type="image/png" href="{{asset('client/img/favicon.png')}}" >
    <link rel="alternate" type="application/rss+xml" title="Docs" href="feed.xml">
    <script src="{{asset('client/js/main.js')}}"></script>
    
  </head>