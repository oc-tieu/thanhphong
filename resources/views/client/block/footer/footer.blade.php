<footer class="uk-section uk-text-center uk-text-muted uk-link-muted">
    <div class="uk-container uk-container-small">
        <div>
            <ul class="uk-subnav uk-flex-center">
                <li class="uk-active"><a href="index.html" >Help</a></li>
                <li><a href="news/index.html" >News</a></li>
                <li><a href="contact/index.html" >Contact</a></li> 
            </ul>
        </div>
        <div class="uk-margin-medium">
            <div data-uk-grid class="uk-child-width-auto uk-grid-small uk-flex-center uk-grid">              
                <div class="uk-first-column">
                    <a href="https://twitter.com/" data-uk-icon="icon: twitter" class="uk-icon-link uk-icon" target="_blank"></a>
                </div>
                <div>
                    <a href="https://www.facebook.com/" data-uk-icon="icon: facebook" class="uk-icon-link uk-icon" target="_blank"></a>
                </div>
                <div>
                    <a href="https://www.instagram.com/" data-uk-icon="icon: instagram" class="uk-icon-link uk-icon" target="_blank"></a>
                </div>
                <div>
                    <a href="https://vimeo.com/" data-uk-icon="icon: vimeo" class="uk-icon-link uk-icon" target="_blank"></a>
                </div>
            </div>
        </div>
        <div class="uk-margin-medium uk-text-small copyright">Made by a <a href="https://ivanchromjak.com/">human</a> somewhere on the planet earth.</div>
    </div>
</footer>