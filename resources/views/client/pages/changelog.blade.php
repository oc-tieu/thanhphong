@extends('client.master')
@section('content')
<div class="uk-section">
    <div class="uk-container uk-container-xsmall">
        <article class="uk-article">
            <h1 class="uk-article-title">Changelog</h1>
            <div class="article-content">
                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
            </div>
            <div class="tm-timeline uk-margin-large-top">
                <div class="tm-timeline-entry">
                    <div class="tm-timeline-time">
                        <h5>Oct 21, 2017</h5>
                    </div>
                    <div class="tm-timeline-body">
                        <h3>Version 1.0.0 <span class="uk-label">STABLE</span></h3>
                        <ul class="uk-list">
                            <li>Added Slideshow component</li>
                            <li>Added style support for radio and minusbox in Firefox</li>
                            <li>Removed class from Section component</li>
                            <li>Allow fullscreen mode for videos in Lightbox</li>
                            <li>Fixed responsive images in modal for IE11</li>
                            <li>Fix Grid and Margin component for cells with no height</li>
                            <li>Larger horizontal padding for form input and textarea</li> 
                        </ul>
                    </div>
                </div>
                 <div class="tm-timeline-entry">
                    <div class="tm-timeline-time">
                        <h5>Sep 01, 2017</h5>
                    </div>
                    <div class="tm-timeline-body">
                        <h3>Version 1.0.0 <span class="uk-label">BETA 1</span></h3>
                        <ul class="uk-list">
                            <li>Allow fullscreen mode for YouTube videos in Lightbox</li>
                            <li>Fix icons not displaying if connected in rapid succession</li>
                            <li>Fix scrollbar jumping in Switcher</li> 
                        </ul>
                    </div>
                </div>
                <div class="tm-timeline-entry">
                    <div class="tm-timeline-time">
                        <h5>Aug 15, 2017</h5>
                    </div>
                    <div class="tm-timeline-body">
                        <h3>Version 0.6.0 <span class="uk-label"></span></h3>
                        <ul class="uk-list">
                            <li>Added style support for radio and checkbox in Firefox</li>
                            <li>Removed class from Section component</li>
                            <li>Add workaround to mitigate the duplicating icons issue</li>
                            <li>Fixed responsive images in modal for IE11</li>
                        </ul>
                    </div>
                </div>
                <div class="tm-timeline-entry">
                    <div class="tm-timeline-time">
                        <h5>Oct 21, 2017</h5>
                    </div>
                    <div class="tm-timeline-body">
                        <h3>Version 0.5.0 <span class="uk-label"></span></h3>
                        <ul class="uk-list">
                            <li>Media options now support any valid media query syntax</li>
                            <li>Added style support for radio and checkbox in Firefox</li>
                            <li>Fix whitespace trimming in dist</li>
                        </ul>
                    </div>
                </div> 
            </div>
        </article>
    </div>
</div>
<div id="offcanvas-docs" data-uk-offcanvas="overlay: true">
    <div class="uk-offcanvas-bar">
        <button class="uk-offcanvas-close" type="button" data-uk-close></button>
        <h5 class="uk-margin-top">Getting Started</h5>
        <ul class="uk-nav uk-nav-default doc-nav">
            <li class=""><a href="../docs/home/index.html">Creating new page in Jekyll and publishing</a></li>
            <li class=""><a href="../docs/quickstart/index.html">Getting a JavaScript console error</a></li>
            <li class=""><a href="../docs/installation/index.html">Getting started with our application</a></li>
            <li class=""><a href="../docs/windows/index.html">Selecting the right fonts and colors</a></li>
        </ul>
        <h5 class="uk-margin-top">Account and Billing</h5>
        <ul class="uk-nav uk-nav-default doc-nav">
            <li class=""><a href="../docs/frontmatter/index.html">Settings up Jekkyl on Github pages</a></li>
            <li class=""><a href="../docs/posts/index.html">Accepted currencies for product billing</a></li>
            <li class=""><a href="../docs/drafts/index.html">Cancelling a website subscription</a></li>
            <li class=""><a href="../docs/pages/index.html">Updating your billing credit card</a></li>
        </ul>
        <h5 class="uk-margin-top">Customization</h5>
        <ul class="uk-nav uk-nav-default doc-nav">
            <li class=""><a href="../docs/usage/index.html">Design tips for choosing great images</a></li>
            <li class=""><a href="../docs/structure/index.html">Upgrading to paid service</a></li>
            <li class=""><a href="../docs/configuration/index.html">Purchasing domain name and DNS setup</a></li>
        </ul>
        <h5 class="uk-margin-top">Troubleshooting</h5>
        <ul class="uk-nav uk-nav-default doc-nav">
            <li class=""><a href="../docs/variables/index.html">Understanding company billing and accounts</a></li>
            <li class=""><a href="../docs/collections/index.html">Category hosting Setting up new domain and page</a></li>
            <li class=""><a href="../docs/assets/index.html">How To Setup Domain SSL With Github Pages</a></li>
        </ul>
    </div>
</div>
<div id="offcanvas" data-uk-offcanvas="flip: true; overlay: true">
    <div class="uk-offcanvas-bar">
        <button class="uk-offcanvas-close" type="button" data-uk-close></button>
            <ul class="uk-nav uk-nav-default">
                <li><a class="uk-logo uk-margin-small-bottom" href="../index.html"><img src="../assets/posts/logo.svg" alt="Docs"></a></li> 
                <li><a href="../index.html" >Help</a></li>    
                <li><a href="../news/index.html" >News</a></li>
                <li><a href="index.html" >Changelog</a></li> 
                <li><a href="../contact/index.html" >Contact</a></li>
            </ul>
            <div class="uk-margin-small-top uk-text-center uk-text-muted uk-link-muted">
                <div data-uk-grid class="uk-child-width-auto uk-grid-small uk-flex-center uk-grid">  
                    <div class="uk-first-column">
                        <a href="https://twitter.com/" data-uk-icon="icon: twitter" class="uk-icon-link uk-icon" target="_blank"></a>
                    </div>
                    <div>
                        <a href="https://www.facebook.com/" data-uk-icon="icon: facebook" class="uk-icon-link uk-icon" target="_blank"></a>
                    </div>
                    <div>
                        <a href="https://www.instagram.com/" data-uk-icon="icon: instagram" class="uk-icon-link uk-icon" target="_blank"></a>
                    </div>
                    <div>
                        <a href="https://vimeo.com/" data-uk-icon="icon: vimeo" class="uk-icon-link uk-icon" target="_blank"></a>
                    </div>
                </div>
            </div>
    </div>
</div>
@endsection