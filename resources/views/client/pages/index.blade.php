@extends('client.master')
@section('content')
    <div class="uk-section section-hero" data-uk-scrollspy="cls: uk-animation-slide-bottom-medium; repeat: true">
        <div class="uk-container uk-container-small">
            <p class="hero-image uk-text-center"><img src="{{asset('client/posts/imac.svg')}}" alt="Hero"></p>
            <h1 class="uk-heading-hero uk-text-center uk-margin-remove-top">How Can We Help?</h1>
            <p class="subtitle uk-text-lead uk-text-center">Search or browse in depth articles and videos on everything Jekyll, from basic theme setup and hosting to customisation and development</p>
            <div class="hero-search">
                <div class="uk-position-relative">
                    <form class="uk-search uk-search-default uk-width-1-1" name="search-hero" onsubmit="return false;">
                        <span data-uk-search-icon></span>
                        <input id="hero-search" class="uk-search-input" type="search" placeholder="Search for answers...">
                    </form>
                    <ul id="hero-search-results" class="uk-position-absolute uk-width-1-1 uk-list"></ul>
                </div>
                <script>
                SimpleJekyllSearch({
                    searchInput: document.getElementById('hero-search'),
                    resultsContainer: document.getElementById('hero-search-results'),
                    noResultsText: '<li>No results found</li>',
                    searchResultTemplate: '<li><a href="{url}">{title}</a></li>',
                    json: '/search.json'
                });
                </script>
            </div>
        </div>
    </div>
    <div class="uk-section">
        <div class="uk-container">
            <h2 class="uk-text-center heading-hero-2">Browse Topics</h2>
            <p class="subtitle uk-text-lead uk-text-center">Get your answers fast, jump to most popular documentation content</p>
            <div class="uk-child-width-1-3@m uk-grid-match uk-text-center uk-margin-medium-top" data-uk-grid>
                <div>
                    <div class="uk-card uk-card-body uk-inline uk-border-rounded">
                    <a class="uk-position-cover" href="{{'docs'}}"></a>
                        <span data-uk-icon="icon: settings; ratio: 2" class=""></span>
                        <h3 class="uk-card-title uk-margin">Getting Started</h3>
                        <p>Get your account up and running in just few easy steps</p> 
                    </div>
                </div>
                <div>
                    <div class="uk-card uk-card-body uk-inline uk-border-rounded">
                        <a class="uk-position-cover" href="docs/frontmatter/index.html"></a>
                        <span data-uk-icon="icon: credit-card; ratio: 2" class=""></span>
                        <h3 class="uk-card-title uk-margin">Account and Billing</h3>
                        <p>Managing your account, creating new users and exporting data</p>
                    </div>
                </div>
                <div>
                    <div class="uk-card uk-card-body uk-inline uk-border-rounded">
                        <a class="uk-position-cover" href="docs/variables/index.html"></a>
                        <span data-uk-icon="icon: cog; ratio: 2" class=""></span>
                        <h3 class="uk-card-title uk-margin">Troubleshooting</h3>
                        <p>Answers to most common configuration issues</p>
                    </div>
                </div>
            </div>
        
        </div>
    </div>
    <div class="uk-section uk-text-center">
        <div class="uk-container uk-container-small">
            <h2 class="heading-hero-2">Popular Articles</h2>
            <ul class="uk-list uk-list-large list-featured uk-child-width-1-2@s" data-uk-grid>
                <li><a href="docs/assets/index.html">How To Setup Domain SSL With Github Pages</a></li>
                <li><a href="docs/collections/index.html">Category hosting Setting up new domain and page</a></li>
                <li><a href="docs/drafts/index.html">Cancelling a website subscription</a></li>
                <li><a href="docs/home/index.html">Creating new page in Jekyll and publishing</a></li>
                <li><a href="docs/installation/index.html">Getting started with our application</a></li>
                <li><a href="docs/posts/index.html">Accepted currencies for product billing</a></li>
                <li><a href="docs/quickstart/index.html">Getting a JavaScript console error</a></li>
                <li><a href="docs/variables/index.html">Understanding company billing and accounts</a></li>
            </ul>
        </div>
    </div>
    <div class="uk-section uk-text-center">
        <div class="uk-container uk-container-small">
            <h2 class="uk-text-center heading-hero-2">Need More?</h2>
            <p class="subtitle uk-text-lead uk-text-center">This section displays optional page content lorem ipsum</p>
            <div class="article-content uk-margin-medium-top"><p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
            </div>
        </div>
    </div>
    <div class="uk-section uk-text-center">
        <div class="uk-container uk-container-small">
            <div data-uk-scrollspy="cls: uk-animation-slide-bottom-medium; repeat: true">
                <h3>Didn't find an answer to your question?</h3>
                <p class="subtitle uk-text-lead uk-text-center">Get in touch with us for details on additional services and custom work pricing</p>
                <a class="uk-button uk-button-primary uk-button-large button-cta" href="contact/index.html">Contact Us</a>
            </div>
        </div>
    </div>
    <div id="offcanvas-docs" data-uk-offcanvas="overlay: true">
        <div class="uk-offcanvas-bar">
            <button class="uk-offcanvas-close" type="button" data-uk-close></button>
            <h5 class="uk-margin-top">Getting Started</h5>
            <ul class="uk-nav uk-nav-default doc-nav">
                <li class=""><a href="docs/home/index.html">Creating new page in Jekyll and publishing</a></li>
                <li class=""><a href="docs/quickstart/index.html">Getting a JavaScript console error</a></li>
                <li class=""><a href="docs/installation/index.html">Getting started with our application</a></li>
                <li class=""><a href="docs/windows/index.html">Selecting the right fonts and colors</a></li>
            </ul>
            <h5 class="uk-margin-top">Account and Billing</h5>
            <ul class="uk-nav uk-nav-default doc-nav">
                <li class=""><a href="docs/frontmatter/index.html">Settings up Jekkyl on Github pages</a></li>
                <li class=""><a href="docs/posts/index.html">Accepted currencies for product billing</a></li>
                <li class=""><a href="docs/drafts/index.html">Cancelling a website subscription</a></li>
                <li class=""><a href="docs/pages/index.html">Updating your billing credit card</a></li>
            </ul>
            <h5 class="uk-margin-top">Customization</h5>
            <ul class="uk-nav uk-nav-default doc-nav">
                <li class=""><a href="docs/usage/index.html">Design tips for choosing great images</a></li>
                <li class=""><a href="docs/structure/index.html">Upgrading to paid service</a></li>
                <li class=""><a href="docs/configuration/index.html">Purchasing domain name and DNS setup</a></li>
            </ul>
            <h5 class="uk-margin-top">Troubleshooting</h5>
            <ul class="uk-nav uk-nav-default doc-nav">
                <li class=""><a href="docs/variables/index.html">Understanding company billing and accounts</a></li>
                <li class=""><a href="docs/collections/index.html">Category hosting Setting up new domain and page</a></li>
                <li class=""><a href="docs/assets/index.html">How To Setup Domain SSL With Github Pages</a></li>
            </ul>
        </div>
    </div>
    <div id="offcanvas" data-uk-offcanvas="flip: true; overlay: true">
        <div class="uk-offcanvas-bar">
            <button class="uk-offcanvas-close" type="button" data-uk-close></button>
            <ul class="uk-nav uk-nav-default">
                <li><a class="uk-logo uk-margin-small-bottom" href="index.html"><img src="assets/posts/logo.svg" alt="Docs"></a></li>    
                <li><a href="index.html" >Help</a></li>   
                <li><a href="news/index.html" >News</a></li> 
                <li><a href="changelog/index.html" >Changelog</a></li>
                <li><a href="contact/index.html" >Contact</a></li>  
            </ul>
            <div class="uk-margin-small-top uk-text-center uk-text-muted uk-link-muted">
                <div data-uk-grid class="uk-child-width-auto uk-grid-small uk-flex-center uk-grid">          
                    <div class="uk-first-column">
                        <a href="https://twitter.com/" data-uk-icon="icon: twitter" class="uk-icon-link uk-icon" target="_blank"></a>
                    </div>
                    <div>
                        <a href="https://www.facebook.com/" data-uk-icon="icon: facebook" class="uk-icon-link uk-icon" target="_blank"></a>
                    </div>
                    <div>
                        <a href="https://www.instagram.com/" data-uk-icon="icon: instagram" class="uk-icon-link uk-icon" target="_blank"></a>
                    </div>
                    <div>
                        <a href="https://vimeo.com/" data-uk-icon="icon: vimeo" class="uk-icon-link uk-icon" target="_blank"></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection