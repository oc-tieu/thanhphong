<!doctype html>
<html class="no-js" lang="zxx">
    
<!-- Mirrored from demo.devitems.com/cigar-v4/cigar/shop.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 26 Feb 2019 01:07:00 GMT -->
<head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Cigar - shop page</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Favicon -->
        <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.png">
		
		<!-- all css here -->
        <link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/bundle.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/plugins.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/responsive.css')}}">
        <script src="{{asset('assets/js/vendor/modernizr-2.8.3.min.js')}}"></script>
    </head>
    <body>
            <!--shop wrapper start-->
<div class="shop_wrapper shop_reverse">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <ul>
                <li><a href="{{route('index')}}">home</a></li>
                    @foreach ($category as $item)
                <li><a href="{{route('category', ['id' => $item->id ])}}">{{$item->name}}</a></li>
                        
                    @endforeach
                </ul>
            <h1>Số Lượng Hàng Còn Trong Kho {{$count}} </h1>
                <div class="shop_tab_product">   
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="large" role="tabpanel">
                            <div class="row">
                                @foreach ($items as $item)
                                <div class="col-lg-4 col-md-6">
                                    <div class="single_product"> 
                                        <div class="product_thumb">
                                        <a href="product-details.html">@foreach($item->imgs as $imgs) <img src="{{asset($imgs->url)    }}" alt="" srcset=""> @endforeach </a>
                                                <div class="btn_quickview">
                                                    <a href="#" data-toggle="modal" data-target="#modal_box"  title="Quick View"><i class="ion-ios-eye"></i></a>
                                            </div>
                                        </div> 
                                        <div class="product_content">   
                                        <h3><a href="{{route('detail', ['id' => $item->id ])}}">{{$item->name}}</a></h3>
                                            <div class="product_price">
                                            <span class="current_price">{{number_format($item->price,0,',','.')}}</span>
                                            </div>
                                        </div>    
                                    </div>
                                </div>
                                @endforeach
                                <ul>
                                    {{-- @for ($i =1; $i = $items->lastPage(); $i=$i++) --}}
                                    <li>
                                    <a href=""></a>
                                    </li>
                                        
                                    {{-- @endfor --}}
                                </ul>
                                {{$items->links()}} 
                            </div>     
                        </div>
                    </div>
                </div>    
            </div>
        </div>
    </div>
</div>
            <!--shop wrapper end-->


		<!-- all js here -->
        <script src="assets/js/vendor/jquery-1.12.0.min.js"></script>
        <script src="assets/js/popper.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/plugins.js"></script>
        <script src="assets/js/main.js"></script>
    </body>

<!-- Mirrored from demo.devitems.com/cigar-v4/cigar/shop.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 26 Feb 2019 01:08:01 GMT -->
</html>
