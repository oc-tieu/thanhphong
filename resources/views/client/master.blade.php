<!DOCTYPE html>
<html lang="en">
@include('client.block.header.head')
@include('client.block.header.header')
<body>
    @yield('content')
</body>
@include('client.block.footer.footer')
</html>