<!doctype html>
<html class="no-js" lang="zxx">
    
<!-- Mirrored from demo.devitems.com/cigar-v4/cigar/product-details.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 26 Feb 2019 01:12:24 GMT -->
<head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Cigar - product details</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Favicon -->
        <link rel="shortcut icon" type="image/x-icon" href="{{asset('assets/img/favicon.png')}}">
		
		<!-- all css here -->
        <link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/bundle.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/plugins.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/responsive.css')}}">
        <script src="{{asset('assets/js/vendor/modernizr-2.8.3.min.js')}}"></script>
    </head>
    <body>
            <div class="single_product_wrapper">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-5 col-md-6">
                            <div class="product_gallery">
                                <div class="tab-content produc_thumb_conatainer">
                                    <div class="tab-pane fade show active" id="p_tab1" role="tabpanel" >
                                        <div class="modal_img">
                                        <a href="#">@foreach($item->imgs as $imgs) <img src="{{asset($imgs->url)    }}" alt="" srcset=""> @endforeach </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-7 col-md-6">
                            <div class="product_details">
                            <h3>{{$item->name}}</h3>
                                <div class="product_price">
                                <span class="current_price">{{number_format($item->price,0,',','.')}}</span>
                                    <span class="old_price">$28.00</span>
                                </div>
                                <div class="product_ratting">
                                    <ul>
                                        <li><a href="#"><i class="ion-star"></i></a></li>
                                        <li><a href="#"><i class="ion-ios-star-outline"></i></a></li>
                                        <li><a href="#"><i class="ion-ios-star-outline"></i></a></li>
                                        <li><a href="#"><i class="ion-ios-star-outline"></i></a></li>
                                        <li><a href="#"><i class="ion-ios-star-outline"></i></a></li>
                                    </ul>
                                    <ul>
                                        <li><a href="#">1 Review</a></li>
                                    </ul>
                                </div>
                               <div class="product_description">
                               <p>{{$item->detail}}</p>
                               </div>
                                <div class="product_details_action">
                                    <h3>Available Options</h3>
                                    <div class="product_stock">
                                        <label>Quantity</label>
                                        <input min="0" max="100" type="number">
                                    </div>
                                    <div class="product_action_link">
                                        <ul>
                                            <li class="product_cart"><a href="#" title="Add to Cart">Add to Cart</a></li>
                                            <li class="add_links"><a href="#" title="Add to Wishlist"><i class="ion-ios-heart-outline"></i> Add to wishlist</a></li>
                                            <li class="add_links"><a href="#" title="Add to Compare"><i class="ion-loop"></i> Add to compare</a></li>
                                        </ul>
                                    </div>
                                    <div class="social_sharing">
                                        <span>Share</span>
                                        <ul>
                                            <li><a href="#" class="bg-facebook" data-toggle="tooltip" title="Facebook"><i class="fa fa-facebook"></i> Share</a></li>
                                            <li><a href="#" class="bg-Tweet" data-toggle="tooltip" title="twitter"><i class="fa fa-twitter"></i> Tweet</a></li>
                                            <li><a href="#" class="bg-google" data-toggle="tooltip" title="google-plus"><i class="fa fa-google-plus"></i> Google+</a></li>
                                            <li><a href="#" class="bg-pinterest" data-toggle="tooltip" title="pinterest"><i class="fa fa-pinterest"></i> Pinterest</a></li>
                                        </ul>
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
		<!-- all js here -->
        <script src="{{asset('assets/js/vendor/jquery-1.12.0.min.js')}}"></script>
        <script src="{{asset('assets/js/popper.js')}}"></script>
        <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
        <script src="{{asset('assets/js/plugins.js')}}"></script>
        <script src="{{asset('assets/js/main.js')}}"></script>
    </body>

<!-- Mirrored from demo.devitems.com/cigar-v4/cigar/product-details.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 26 Feb 2019 01:12:28 GMT -->
</html>
