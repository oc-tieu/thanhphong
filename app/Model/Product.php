<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table    ="product";
    protected $fillable =['name', 'price', 'intro', 'detail', 'category_id', 'created_at', 'updated_at'];


    public function imgs(){
        return $this->hasMany('App\Model\Img');
    }
    public function categorys() {
        return $this->belongsTo('App\Admin\category');
    }
    
}
