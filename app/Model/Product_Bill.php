<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Product_Bill extends Model
{
    protected $table    ='product_bill';
    protected $fillable =['product_id', 'bill_id', 'created_at', 'updated_at'];
}
