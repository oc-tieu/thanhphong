<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Bill extends Model
{
    protected $table    ='bill';
    protected $fillable =['total', 'users_id', 'created_at', 'updated_at'];


     //quan hệ một nhiều với group
     public function users() {
        return $this->belongsTo('App\Model\Users');
    }
}
