<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Img extends Model
{
    protected $table ="img";
    protected $fillable = ['name', 'url', 'product_id', 'created_at', 'updated_at'];

    public function products(){
        return $this->belongsTo('App\Admin\Product');
    }
}
