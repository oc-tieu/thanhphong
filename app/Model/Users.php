<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    protected $table = 'users';
    protected $fillable = ['username', 'email', 'passworld', 'group_id', 'created_at', 'updated_at'];

    
    public function group() {
        return $this->belongsTo('App\Model\Group');
    }
    
    public function bill() {
        return $this->hasMany('App\Model\Bill');
    }
}
