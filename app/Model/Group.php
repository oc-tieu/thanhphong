<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $table ='group';
    protected $fillabble = ['title', 'created_at', 'updated_at'];
    
    // quan hệ nhiều một vs user
    public function user() {
        return $this->hasMany('App\Model\User');
    }
}
