<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Product;
use App\Model\Img;
use App\Model\Category;
class HomeController extends Controller
{
    private $product;
    private $img;
    public function __construct(Product $product, Img $img, Category $category)
    {
        $this->product     = $product;
        $this->img         = $img;
        $this->category    = $category;
    }
    public function index(){
        $data['category'] = $category = category::all();
        
        $data['new'] = $this->product->with('imgs')->orderBy('id','desc')->get();
        return view('client.index', $data);
    }
    public function getDetail($id){
        // $getData =$this->product->with('imgs')->where('id',$id)->first();
        // $data['product'] = $this->product->$getData($id);
        $data['item'] = Product::find($id);
        return view('client.detail',$data);
    }
    public function category($id){
        $list_img_array = array();
        $data['category'] = $category = Category::all();
        $data['new'] = $this->product->with('imgs')->orderBy('id','desc')->get();
        //lấy danh  sách sản phẩm theo category
        $data['items'] = Product:: where('category_id',$id)->orderBy('id','desc')->paginate(5);
        // $data['items'] = Product:: paginate(5);
        $data['count'] = Product:: where('category_id',$id)->count();

        return view('client.category_p', $data);
    }
    
}
