<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Category;
use App\Model\Product;
use App\Model\Img;
class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $category;
    public function __construct(Category $category, Product $product, Img $img){
        $this->category = $category;
        $this->product = $product;
        $this->img = $img;
        // $this->related = ['username', 'email', 'passworld'];
    }
    public function index()
    {
        $product = $this->product->with('imgs')->get();
        return view('admin.product.index', compact('product') );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = category::all();
        return view('admin.product.create',compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $destination = 'uploads/photos/'; // your upload folder
        $image       = $request->file('url');
        $filename    = $image->getClientOriginalName(); // get the filename
        $image->move($destination, $filename); // move file to destination
        // create a record
        $product = $this->product->create($request->input());
        $img = Img::create(['product_id' => $product->id,'url' => $destination . $filename,]);
    
       
        return redirect()->route('admin.product.index');


        //  $product->cats()->sync($request->cats, false);
        //  Session::flash('flash_message', 'Service successfully added!');
        //  return redirect()->back()->with('success', 'Service Successfully Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = $this->product->with('imgs')->find($id);
        return view('admin.product.edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product=$this->product->findOrFail($id);
        $product->update($request->only(['name', 'price', 'intro', 'detail']));
        // $product->update([
        //     'name'=>$request->input('name'),
        //     'price' => $request->input('price'),
        //     'intro' => $request->input('intro'),
        //     'detail' => $request->input('detail'),
        // ]);
        $destination = 'uploads/photos/'; // your upload folder
        $image       = $request->file('url');
        $filename    = $image->getClientOriginalName(); // get the filename
        $image->move($destination, $filename); // move file to destination

        $img = $this->img->where('product_id', $id)->first();
        $this->img->where('id', $img->id)->update([
            'url' => $destination . $filename,
        ]);
        return redirect()->route('admin.product.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $list_img_array = array();
        $img = Img::where('product_id', $id)->get();
        foreach($img as $item) {
            $list_img_array[] = $item->img_id;
        }
        Img::where('product_id', $id)->delete();
        $product     = $this->product->destroy($id);
        $delete_img = $this->img->whereIn('id',$list_img_array)->delete();
        return redirect()->route('admin.product.index');
        
    }
}
