<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::group(['namespace'=>'Admin'], function () {
    Route::group(['prefix' => 'admin'], function () {
        Route::resource('user', 'UserController');
        Route::resource('category', 'CategoryController');
        Route::resource('product', 'ProductController');
    });
});
Route::get('/home','Client\HomeController@index')->name('index');
Route::get('/detail/{id}','Client\HomeController@getDetail')->name('detail');

Route::get('/category/{id}','Client\HomeController@category')->name('category');